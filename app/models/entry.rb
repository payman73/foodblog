class Entry < ApplicationRecord
  validates_presence_of :calories , :proteins , :carbohydrates , :meal_type , :fats , :category_id
  belongs_to :category

  def day
    self.created_at.strftime("%b %e , %Y")
  end
end
